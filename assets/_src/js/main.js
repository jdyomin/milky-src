$(function() {
    $('.js-points-block-toggle').on('click', function() {
        $(this).parents('.points-block').toggleClass('opened');
    });

    $('.js-account-toggle').on('click', function() {
        $(this).parents('.account-block').toggleClass('unroll');
    });

    $(document).mouseup(function (e) {
        var tooltips = $('.js-tooltip');
        if (tooltips.has(e.target).length === 0){
            tooltips.removeClass('opened');
        }
    });

    $('.custom-select').selectric({
        multiple: {
            separator: ', ',
            keepMenuOpen: true,
            maxLabelEntries: false
        },
        maxHeight: 250
        //labelBuilder: 'Выберите из списка...'
    });

    $('.js-codes-trigger').on('click', function() {
        $('.active-codes').toggleClass('opened');
    });

    $('.js-question-trigger').on('click', function() {
        var el = $(this).closest('.js-question');

        if (!el.is('.opened')) {
            $('.js-question').removeClass('opened');
            el.toggleClass('opened');
        }
    });

    $('.js-menu-trigger').on('click', function() {
        $(this).closest('.page-header').toggleClass('menu-opened');
    });


    $(window).on('scroll', setMenu);

    function setMenu() {
        var header = $('.page-header');

        if ($(window).width() < 1024) return false;

        if ($(window).scrollTop() > 100) {
            header.addClass('_anim');
        } else {
            header.removeClass('_anim');
        }
    }

    var bannerSwiperContainer = '.banner-slider__container',
        bannerSwiper = new Swiper(bannerSwiperContainer, {
        loop: true,
        autoplay: 5000,
        speed: 800,
        pagination: $(bannerSwiperContainer).find('.swiper-pagination')[0],
        paginationClickable: true,
        nextButton: $(bannerSwiperContainer).find('.swiper-button-next')[0],
        prevButton: $(bannerSwiperContainer).find('.swiper-button-prev')[0]
    });
});