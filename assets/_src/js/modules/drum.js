$(function() {
    var playBtn = $('.js-play-drum');

    playBtn.on('click', function () {
        playDrum(1);
    });


    function playDrum(pos, time) {
        var drum = $('.js-drum'),
            drumLine = drum.find('.js-drum-line'),
            drumPack = drumLine.find('.js-line-item[data-target-pos="'+pos+'"]'),
            drumPackW = drumPack.width(),
            pin = drum.find('.pin'),
            drumLineML = parseFloat(drumLine.css('margin-left')),
            pinML = parseFloat(pin.css('margin-left')),
            leftGut = (pinML - drumLineML) * -1,
            dur = time || 5,
            minPos,
            maxPos,
            finalPos;

        if (!drum.length) return;

        window.drumPackPos = pos;

        playBtn.hide();

        minPos = parseFloat(drumPack.position().left) + leftGut - (drumPackW / 2);
        maxPos = parseFloat(drumPack.position().left) + leftGut + (drumPackW / 2);
        finalPos = getRandom(minPos, maxPos) * -1;

        animateDrum(finalPos);

        function getRandom(min, max) {
            return Math.floor(Math.random()*(max-min+1)+min);
        }

        function animateDrum(position) {
            var tl = new TimelineMax();

            if ($(window).width() > 1024) {
                $('.pin').addClass('_anim');
            }

            tl.to(drumLine, dur, {x: position, ease:Power2.easeOut});
        }
    }

    $(window).on('resize', function () {
        if (window.drumPackPos && $('.js-drum').length) {
            playDrum(window.drumPackPos, 0.1);
        }
    });
});