$(function() {
    var outerBlock = $('.points-slider'),
        swiperSliderMob = outerBlock.find('.mobile-slider'),
        swiperSlideMob = outerBlock.find('.mobile-slide'),
        swiperWrap = outerBlock.find('.mobile-slider-wrapper'),
        myMobSwiper;

    if (swiperSliderMob.length){
        $(window).on('resize', function () {
            if ($(window).width() < 1006) {
                initSlider();
            } else {
                destroySlider();
            }
        });

        if ($(window).width() < 1006) {
            initSlider();
        }
    }

    function initSlider() {
        swiperSliderMob.addClass('swiper-container');
        swiperWrap.addClass('swiper-wrapper');
        swiperSlideMob.addClass('swiper-slide');

        swiperSliderMob.each(function() {
            var options = $(this).data('options') || {},
                swiperMobOptions,
                swiperMobDefaults = {
                    loop: true,
                    pagination: '.swiper-pagination',
                    paginationClickable: true
                };

            swiperMobOptions = $.extend(swiperMobDefaults, options);
            myMobSwiper = new Swiper(this, swiperMobOptions);
        });
    }

    function destroySlider() {
        myMobSwiper.destroy(true, true);
        swiperSliderMob.removeClass('swiper-container');
        swiperWrap.removeClass('swiper-wrapper');
        swiperSlideMob.removeClass('swiper-slide');
    }
});