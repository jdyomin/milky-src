$(function() {
    var outerBlock = $('.winners'),
        swiperSlider = outerBlock.find('.swiper-container'),
        swiperSliderMob = outerBlock.find('.mobile-slider'),
        swiperSlideMob = outerBlock.find('.mobile-slide'),
        swiperWrap = outerBlock.find('.mobile-slider-wrapper'),
        myMobSwiper;

    swiperSlider.each(function() {
        var options = $(this).data('options') || {},
            $parent = $(this).parent(),
            swiperDefaults = {
                nextButton: $parent.find('.swiper-button-next')[0],
                prevButton: $parent.find('.swiper-button-prev')[0],
                observer: true,
                simulateTouch: false
            };

        var swiperOptions = $.extend(swiperDefaults, options),
            mySwiper = new Swiper(this, swiperOptions);
    });

    if (swiperSliderMob.length){
        $(window).on('resize', function () {
            if ($(window).width() < 1006) {
                initSlider();
            } else {
                destroySlider();
            }
        });

        if ($(window).width() < 1006) {
            initSlider();
        }
    }

    function initSlider() {
        swiperSliderMob.addClass('swiper-container');
        swiperWrap.addClass('swiper-wrapper');
        swiperSlideMob.addClass('swiper-slide');

        swiperSliderMob.each(function() {
            var options = $(this).data('options') || {},
                swiperMobOptions,
                swiperMobDefaults = {
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    simulateTouch: false
                };

            swiperMobOptions = $.extend(swiperMobDefaults, options);
            myMobSwiper = new Swiper(this, swiperMobOptions);
        });
    }

    function destroySlider() {
         myMobSwiper.destroy(true, true);
        swiperSliderMob.removeClass('swiper-container');
        swiperWrap.removeClass('swiper-wrapper');
        swiperSlideMob.removeClass('swiper-slide');
    }

    var pagerEl = $('.winner-slider-pagination span'),
        sliderTrigers = $('.winners__col').find('.swiper-button-next, .swiper-button-prev');

    pagerEl.on('click', function() {
        var n = $(this).index();
        if (!( $(this).hasClass('active'))) {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');

            var visibleSliderPaginationBullet = $(this).parents('.winners__col').find('.swiper-slide-active .mobile-slider-pagination .swiper-pagination-bullet');
            $.each(visibleSliderPaginationBullet, function() {
                if ($(this).index() == n) {
                    $(this).click();
                }
            })
        }
    });

    sliderTrigers.on('click', function() {
        var bullets = $(this).parents('.winners__col').find('.winner-slider-pagination span');
        bullets.removeClass('active');
        bullets.eq(0).addClass('active');
    });
});